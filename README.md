# dotfiles managed using GNU stow

Basic usage:

```bash
stow $DIR
```

writes symbolic links for all the dotfiles in `$DIR` to your home directory.

## Bootstrapping

### MacOS

- Clone this repository
- Install xcode command line tools
- Install homebrew
- Using homebrew, install:
  * `stow`
  * `bash` (the built-in one is super outdated)
  * `bash-completion`
