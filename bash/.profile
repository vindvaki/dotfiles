################################################################################
# Path & environment
################################################################################

# user bin path
export PATH="$PATH:$HOME/.local/bin:$HOME/bin"

# Rust / Cargo
export PATH="$HOME/.cargo/bin:$PATH"
# command -v sccache >/dev/null 2>&1 && export RUSTC_WRAPPER=sccache

# use macvim's vim in the terminal
if [ -f /usr/local/bin/mvim ]; then
  alias vim="mvim -v"
fi


################################################################################
# Completions
################################################################################

# support both homebrew and linux default completion setup paths
COMPLETION_FILE_PATHS="
  /usr/local/etc/bash_completion
  /usr/share/bash-completion/bash_completion
  /usr/local/share/bash-completion/bash_completion
  /usr/share/git-core/contrib/completion/git-prompt.sh
  "

for completion_path in $COMPLETION_FILE_PATHS; do
  if [ -f $completion_path ]; then
    source $completion_path
  fi
done

################################################################################
# Appearance
################################################################################

export TERM=xterm-256color
export CLICOLOR=1

# tput setaf $color -- set foreground
# tput setab $color -- set background
# tput bold         -- make bold
# tput sgr0         -- reset formatting

rainbow() {
  color=0
  for row in {0..15}; do
    for col in {0..15}; do
      tput setab $color
      printf " %3d " $color
      color=$(( $color + 1 ))
    done
    echo
  done
  tput sgr0
}

__fmt_bold="\[$(tput bold)\]"
__fmt_fg_red="\[$(tput setaf 1)\]"
__fmt_fg_gold="\[$(tput setaf 3)\]"
__fmt_fg_dark="\[$(tput setaf 239)\]"
__fmt_fg_white="\[$(tput setaf 255)\]"
__fmt_reset="\[$(tput sgr0)\]"

export PROMPT_DIRTRIM=2
export PS1="${__fmt_bold}${__fmt_fg_gold}\u${__fmt_fg_dark}@${__fmt_fg_red}\h ${__fmt_fg_white}\w${__fmt_fg_dark}"'$(__git_ps1)'" \$${__fmt_reset} "
