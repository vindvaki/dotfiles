""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Bare minimum
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Options that we want before plugins in case a plugin fails to load (we want
" to be able to use vim without ESC on the iPad)

set nocompatible
inoremap jk <esc>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" To install vim-plug:
"
"   curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
" See https://github.com/junegunn/vim-plug

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible' " sane defaults
Plug 'sheerun/vim-polyglot' " all the languages
Plug 'wincent/ferret' " grep in project
Plug 'vim-airline/vim-airline' " fancy modeline
Plug 'vim-airline/vim-airline-themes'
Plug 'flazz/vim-colorschemes' " more themes
Plug 'terryma/vim-multiple-cursors' " multiple cursors
Plug 'scrooloose/nerdtree' " file sidebar
Plug 'ntpeters/vim-better-whitespace' " trim trailing whitespace on save etc
Plug 'junegunn/goyo.vim' " distraction free writing
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' } " fzf binary
Plug 'junegunn/fzf.vim' " fzf vim extras
Plug 'Raimondi/delimitMate' " smart auto completion of parens, quotes etc

call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fundamental settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set bg=dark
colorscheme PaperColor

"" Basic features
syntax on
set hidden       " hide buffer on :q (don't close)
set wildmenu     " tab completion for the command line
set scrolloff=3  " Maintain more context around the cursor
"set cursorline  " Disbled because it's
if &term =~ '256color'
  " fix mosh tmux background color
  set t_ut=
endif

"" Search settings
set hlsearch   " Highlight search terms...
set incsearch  " ... dynamically as they are typed
set ignorecase " Don't do case sensitive searches
set smartcase  " ... rather do smarter case search

"" Format options
set formatoptions+=cqro " what is this?!

"" Indentation
set tabstop=2    " width of tab
set shiftwidth=2 " default indent
set expandtab    " spaces instead of tabs
set autoindent   " very basic indenting, not smart at all

"" Plugin: fzf
nnoremap <Leader>t :Files<cr>
nnoremap <Leader>g :GFiles<cr>
nnoremap <Leader>a :Rg<cr>
nnoremap <Leader>b :Buffers<cr>
nnoremap <Leader>c :Commands<cr>

"" Plugin: vim-better-whitespace
autocmd BufEnter * EnableStripWhitespaceOnSave

"" Plugin: vim-multiple-cursors
let g:multi_cursor_quit_key='<CR>'

"" Filetype: Markdown
let g:vim_markdown_folding_disabled = 1
